package com.programcreek.helloworld.controller;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {
		String message = "Visit";
		
		@RequestMapping("/hello")
		public ModelAndView showMessage(@RequestParam(value = "url", required = true) String url ) throws ClientProtocolException, IOException{
				System.out.println("in controller");
				
				ModelAndView mv = new ModelAndView("helloworld");
				try {
					StatusLine result = pingUrl(url);
					mv.addObject("message", result.getReasonPhrase());
					mv.addObject("code", result.getStatusCode());
					mv.addObject("url", url);
					
				} catch (IOException e) {
				
					mv.addObject("message", e.toString());
					mv.addObject("code", e.getCause());
					mv.addObject("url", url);
				} 
				
				
				return mv;
				
		}
		
	

		private StatusLine pingUrl(String url) throws ClientProtocolException, IOException {

			RequestConfig.Builder requestBuilder = RequestConfig.custom();
			requestBuilder = requestBuilder.setConnectTimeout(5000);
			requestBuilder = requestBuilder.setConnectionRequestTimeout(5000);
			HttpClientBuilder builder = HttpClientBuilder.create();     
			builder.setDefaultRequestConfig(requestBuilder.build());
			HttpClient client = builder.build();
			HttpGet request = new HttpGet(url);

			// add request header
			request.addHeader("User-Agent", "User-Agent");
			HttpResponse response = client.execute(request);

			return response.getStatusLine();
		}
		

//

		
}
