<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mes test Bootstrap</title>

    <!-- Bootstrap -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">

 
  </head>
  <body>
  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bootstrap</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#help">Help</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div class="starter-template">
      <br></br>
        <h1>Test sur Bootstrap</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
   <c:set var="contextPath" value="${pageContext.request.contextPath}"/>

    <table class="table table-striped">
   <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>URL Name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td><a href="${contextPath}/hello?url=http://google.fr">Google URL test</a></td>   
    </tr>
    <tr>
      <th scope="row">2</th>
      <td><a href="${contextPath}/hello?url=http://aol.fr">AOL URL test</a></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td><a href="${contextPath}/hello?url=http://yahoo.fr">Yahoo URL test</a></td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td><a href="${contextPath}/hello?url=http://mabrouk.cobra.fr">Mabrouk URL test</a></td>
    </tr>
     <tr>
      <th scope="row">5</th>
      <td><a href="${contextPath}/hello?url=http://google.fr:81">Google port 81 URL test</a></td>
    </tr>
    <tr>
      <th scope="row">6</th>
      <td><a href="${contextPath}/hello?url=http://google.fr/notapage">Google 404 URL test</a></td>
    </tr>
  </tbody>
</table>
</div>

    </div><!-- /.container -->
    
    
    
   
  


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="resources/js/bootstrap.min.js"></script>
  </body>
</html>