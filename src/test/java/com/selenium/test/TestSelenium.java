package com.selenium.test;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestSelenium {
	public static void main(String[] args) {

		// create intsance of firefox driver
		WebDriver driver = new FirefoxDriver();

		// Visit google
		driver.get("http://localhost:8080/HelloWorld/");
		// or driver.navigate().to("http://localhost:8080/HelloWorld/");
		WebElement home = driver.findElement(By.linkText("Home"));

		// Find the element for test
		List<WebElement> elements = driver.findElements(By.tagName("a"));
		for (WebElement webElement : elements) {
			System.out.println(webElement.getText());
		}

		WebElement elementTemp = driver.findElement(By.xpath("//a[1]"));
		System.out.println(" le second : " + elementTemp.getText());

		int count = elements.size();

		for (int i = 0; i < count; i++) {
			// find the nth element a
//			WebElement element = driver.findElement(By.xpath("//a[" + (i + 1) + "]"));
			WebElement element = driver.findElements(By.tagName("a")).get(i);
			// click on it
			element.click();
			// find the home
			home = driver.findElement(By.linkText("Home"));
			// click on it
			home.click();
		}



		

		System.out.println("Page title is : " + driver.getTitle());

		driver.close();

	}

}
